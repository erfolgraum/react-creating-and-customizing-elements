import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
// import App from './App';

const root = ReactDOM.createRoot(document.getElementById("root"));

const planetList = (
  <ul className="planets-list">
    <li>Mercury</li>
    <li>Venus</li>
    <li>Earth</li>
    <li>Mars</li>
    <li>Jupiter</li>
    <li>Saturn</li>
    <li>Uranus</li>
    <li>Neptune</li>
  </ul>
);

root.render([
  React.createElement(
    "div",
    {},
    React.createElement(
      "h1",
      { style: { color: "#999", fontSize: "19px" } },
      "Solar system planets"
    ),
  ),
  planetList,
]);
